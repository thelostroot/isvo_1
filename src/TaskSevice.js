import PeriodicTable from './PeriodicTable';

export const generateTask = (count) => {

    const tasks = [];

    for (let i = 0; i < count; i++) {

        // выбираем период
        const period = getRandom(2, 8);

        // берем три элемента с этого периода
        let periodElements = PeriodicTable.filter(x => x.period == period);
        const variants = [];
        let indexs = getRandom(0, periodElements.length, 3);
        for (let j = 0; j < 3; j++) {
            variants.push(periodElements[indexs[j]]);
        }

        // берем еще 2 любых элемента с таблицы
        let otherPeriodElements = PeriodicTable.filter(x => x.period != period);
        indexs = getRandom(0, periodElements.length, 3);
        for (let j = 0; j < 2; j++) {
            variants.push(otherPeriodElements[indexs[j]]);
        }

        tasks.push({
            id: i,
            variants: shuffleArray(variants),
            answer: []
        });
    }

    return tasks;
    return [{
        id: getRandom(0, 100000),
        variants: [{
            symbol: getRandom(0, 100000),
            number: getRandom(0, 100000),
            period: getRandom(0, 100000),
        }],
        answer: []
    }]
}

const existInArray = (array, item) => {
    return array.findIndex(x => x == item) != -1;
}

const getRandom = (min, max, count) => {
    if (!count) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    const result = [];
    for (let i = 0; i < count; i++) {
        let randomNumber = null;
        do {
            randomNumber = Math.floor(Math.random() * (max - min) + min);
        } while (existInArray(result, randomNumber));
        result.push(randomNumber);
    }
    return result;
}

const shuffleArray = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}